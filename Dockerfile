FROM python:3.10

WORKDIR /simple-python-docker

COPY . /simple-python-docker

RUN pip install -r requirements.txt

ENTRYPOINT [ "python3", "app.py" ]

