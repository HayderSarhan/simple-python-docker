from flask import Flask

app = Flask(__name__)

@app.route('/')

def Home():
    return "<h1>Task 5</h1> <p>This is a simple web app created using Flask and Docker</p>"

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)